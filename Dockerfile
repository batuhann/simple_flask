FROM python:3.8
RUN pip3 install flask
RUN pip3 install gunicorn
COPY . /app
WORKDIR /app
EXPOSE 8000
CMD gunicorn app:app --bind 0.0.0.0:8000 -w 5 --threads 3
